using payment_api.API.Data;
using payment_api.API.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using payment_api.Enum;
using payment_api.Model.Order;
using Microsoft.Extensions.Logging;
using Moq;
using Microsoft.Extensions.Options;
using payment_api.Interface;

public class OrderServiceTests
{
    [Fact]
    public void AddOrder_ShouldAddOrderSuccessfully()
    {
        // Arrange
        var options = new DbContextOptionsBuilder<MemoryDbContext>()
            .UseInMemoryDatabase(databaseName: "AddOrderDatabase")
            .Options;

        using (var context = new MemoryDbContext(options))
        {
            var memoryCache = new MemoryCache(new MemoryCacheOptions());
            var logger = new LoggerFactory().CreateLogger<OrderService>();

            var orderService = new OrderService(context, memoryCache, logger);

            var orderId = Guid.NewGuid();
            var orderViewModel = new OrderViewModel
            {
                Id = Guid.NewGuid(),
                Date = DateTime.Now,
                Status = StatusEnum.Aguardando_pagamento,
                Seller = new SellerViewModel
                {
                    Id = Guid.NewGuid(),
                    Name = "Teste 1",
                    Cpf = "9999999999",
                    Email = "Teste@teste.com",
                    Phone = "3388888888"
                },
                Itens = new List<ItemsViewModel>
                {
                        new ItemsViewModel
                        {
                            Id = Guid.NewGuid(),
                            Name = "Item 1",
                            Amount = 1,
                            Value = 19.99m,
                        },
                        new ItemsViewModel
                        {
                            Id = Guid.NewGuid(),
                            Name = "Item 2",
                            Amount = 2,
                            Value = 29.99m,
                        }

                }
            };


            // Act
            var result = orderService.AddOrder(orderId, orderViewModel);

            // Assert
            Assert.NotNull(result);
            Assert.Single(result);

        }
    }

    [Fact]
    public void UpdateOrderStatus_ShouldUpdateStatusSuccessfully()
    {
        // Arrange
        var options = new DbContextOptionsBuilder<MemoryDbContext>()
            .UseInMemoryDatabase(databaseName: "UpdateOrderStatusDatabase")
            .Options;

        using (var context = new MemoryDbContext(options))
        {
            var memoryCache = new MemoryCache(new MemoryCacheOptions());
            var logger = new LoggerFactory().CreateLogger<OrderService>();

            var orderService = new OrderService(context, memoryCache, logger);

            var orderId = Guid.NewGuid();
            var initialOrderViewModel = new OrderViewModel
            {
                Id = Guid.NewGuid(),
                Date = DateTime.Now,
                Status = StatusEnum.Aguardando_pagamento,
                Seller = new SellerViewModel
                {
                    Id = Guid.NewGuid(),
                    Name = "Teste 1",
                    Cpf = "9999999999",
                    Email = "Teste@teste.com",
                    Phone = "3388888888"
                },
                Itens = new List<ItemsViewModel>
                {
                        new ItemsViewModel
                        {
                            Id = Guid.NewGuid(),
                            Name = "Item 1",
                            Amount = 1,
                            Value = 19.99m,
                        },
                        new ItemsViewModel
                        {
                            Id = Guid.NewGuid(),
                            Name = "Item 2",
                            Amount = 2,
                            Value = 29.99m,
                        }

                }
            };
            orderService.AddOrder(orderId, initialOrderViewModel);

            // Act
            var result = orderService.UpdateOrderStatus(orderId, StatusEnum.Pagamento_aprovado);

            // Assert
            Assert.True(result);
        }
    }

    [Fact]
    public void AddOrder_ShouldNotAllowEmptyItemList()
    {
        // Arrange
        var options = new DbContextOptionsBuilder<MemoryDbContext>()
            .UseInMemoryDatabase(databaseName: "UpdateOrderStatusDatabase")
            .Options;

        using (var context = new MemoryDbContext(options))
        {
            var memoryCache = new MemoryCache(new MemoryCacheOptions());
            var logger = new LoggerFactory().CreateLogger<OrderService>();

            var orderService = new OrderService(context, memoryCache, logger);

            var orderViewModelWithoutItems = new OrderViewModel
            {
                Id = Guid.NewGuid(),
                Date = DateTime.Now,
                Status = StatusEnum.Aguardando_pagamento,
                Seller = new SellerViewModel
                {
                    Id = Guid.NewGuid(),
                    Name = "Test Seller",
                    Cpf = "123456789",
                    Email = "seller@test.com",
                    Phone = "123456789"
                },
                Itens = null
            };

            // Act and Assert
            Assert.Throws<InvalidOperationException>(() => orderService.AddOrder(Guid.NewGuid(), orderViewModelWithoutItems));
        }
    }

}
