﻿using payment_api.Enum;
using payment_api.Model.Order;

namespace payment_api.Interface
{
    public interface IOrderService
    {
        IEnumerable<Order> AddOrder(Guid id, OrderViewModel OrderViewModel);
        bool UpdateOrderStatus(Guid id, StatusEnum newStatus);
        Order GetOrderById(Guid id);
        IEnumerable<Order> GetOrders();
        bool RemoveOrder(Guid id);
    }
}
