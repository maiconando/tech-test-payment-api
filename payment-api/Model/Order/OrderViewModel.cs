﻿using payment_api.Enum;
using payment_api.Model.Order;

namespace payment_api.Model.Order
{
    public class OrderViewModel
    {
        public Guid Id { get; set; }
        public SellerViewModel? Seller { get; set; }
        public List<ItemsViewModel>? Itens { get; set; }
        public StatusEnum Status { get; set; }
        public DateTime Date { get; set; }
    }
}