﻿using payment_api.Enum;

namespace payment_api.Model.Order
{
    public class ItemsViewModel
    {
        public Guid Id { get; set; } 
        public string Name { get; set; } = string.Empty;
        public int Amount { get; set; }
        public decimal Value { get; set; }

    }

}
