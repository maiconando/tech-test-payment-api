﻿using System.Text.Json.Serialization;

namespace payment_api.Model.Order
{
    public class Order
    {
        public Order()
        {
            Orders = new List<Order>();
        }

        public Order(Guid id, OrderViewModel orderViewModel)
        {
            Id = id;
            OrderViewModel = orderViewModel;
        }

        [JsonIgnore]
        public List<Order> Orders { get; set; }
        public Guid Id { get; set; }
        public OrderViewModel OrderViewModel { get; set; }
    }
}