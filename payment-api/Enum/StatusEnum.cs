﻿namespace payment_api.Enum
{
    public enum StatusEnum
    {
        Aguardando_pagamento = 0,
        Pagamento_aprovado = 1,
        Enviado_transportadora = 2,
        Entregue = 3,
        Cancelado = 4
    }
}
