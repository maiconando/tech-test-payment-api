using System.Text.Json.Serialization;
using payment_api.API.Data;
using payment_api.API.Services;
using Microsoft.EntityFrameworkCore;
using payment_api.Interface;
using Microsoft.OpenApi.Models;
using System.ComponentModel;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<MemoryDbContext>(options =>
    options.UseInMemoryDatabase("MemoryDatabase"));

builder.Services.AddScoped<MemoryDbContext, MemoryDbContext>();
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddMemoryCache();
builder.Services.AddSwaggerGen();
builder.Services.AddScoped<IOrderService, OrderService>();

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddControllers()
    .AddJsonOptions(options =>
    {
        options.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
    });

// Configurando o servi�o de documenta��o do Swagger
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1",
        new OpenApiInfo
        {
            Title = "payment-api",
            Version = "v1",
            Description = "API REST criada com o ASP.NET Core",
            Contact = new OpenApiContact
            {
                Name = "Maicon F Paix�o",
                Url = new Uri("https://opensource.org/licenses/MIT") 
            }
        });

});
       


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
