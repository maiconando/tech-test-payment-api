﻿using Microsoft.AspNetCore.Mvc;
using payment_api.Enum;
using System;
using System.Diagnostics;
using payment_api.Interface;
using payment_api.Model.Order;

namespace payment_api.API.Controllers
{
    /// <summary>
    /// Controller responsável por lidar com operações relacionadas a pedidos (orders).
    /// </summary>
    [ApiController]
    [Route("api/Orders")]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService _OrderService;

        /// <summary>
        /// Construtor da classe OrdersController.
        /// </summary>
        /// <param name="OrderService">Instância de serviço de pedidos.</param>
        public OrdersController(IOrderService OrderService)
        {
            _OrderService = OrderService ?? throw new ArgumentNullException(nameof(OrderService));
        }

        /// <summary>
        /// Obtém um pedido pelo ID.
        /// </summary>
        /// <param name="id">ID único do pedido.</param>
        /// <returns>Objeto representando o pedido encontrado.</returns>
        [HttpGet("{id:guid}")]
        public IActionResult GetOrderById(Guid id)
        {
            var order = _OrderService.GetOrderById(id);

            if (order == null)
            {
                return NotFound(new { Message = "Order not found." });
            }

            return Ok(order);
        }

        /// <summary>
        /// Obtém todos os pedidos.
        /// </summary>
        /// <returns>Lista de pedidos.</returns>
        [HttpGet]
        public IActionResult GetOrders()
        {
            var watch = Stopwatch.StartNew();

            var Orders = _OrderService.GetOrders();

            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;

            return Ok(new { ResponseTime = $"Response Time => {elapsedMs} ms", Orders });
        }

        /// <summary>
        /// Adiciona um novo pedido.
        /// </summary>
        /// <param name="OrderViewModel">Informações do pedido a ser adicionado.</param>
        /// <returns>Resultado da operação de adição.</returns>
        [HttpPost]
        public IActionResult AddOrders(OrderViewModel OrderViewModel)
        {
            return Ok(_OrderService.AddOrder(Guid.NewGuid(), OrderViewModel));
        }

        /// <summary>
        /// Atualiza o status de um pedido.
        /// </summary>
        /// <param name="id">ID único do pedido.</param>
        /// <param name="statusUpdate">Novo status desejado.</param>
        /// <returns>Resultado da operação de atualização de status.</returns>
        [HttpPatch("{id:guid}/update-status")]
        public IActionResult UpdateOrderStatus(Guid id, [FromBody] StatusEnum statusUpdate)
        {
            var success = _OrderService.UpdateOrderStatus(id, statusUpdate);

            if (success)
            {
                return Ok(new { Message = "Order status updated successfully." });
            }
            else
            {
                return NotFound(new { Message = "Order not found or invalid status transition." });
            }
        }

        /// <summary>
        /// Remove um pedido pelo ID.
        /// </summary>
        /// <param name="id">ID único do pedido a ser removido.</param>
        /// <returns>Resultado da operação de remoção.</returns>
        [HttpDelete("{id:guid}")]
        public IActionResult RemoveOrders(Guid id)
        {
            var success = _OrderService.RemoveOrder(id);

            if (success)
            {
                return Ok(new { Message = "Order removed successfully." });
            }
            else
            {
                return NotFound(new { Message = "Order not found." });
            }
        }
    }
}
