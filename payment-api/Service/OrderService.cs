﻿using payment_api.API.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using payment_api.Enum;
using payment_api.Model.Order;
using payment_api.Interface;

namespace payment_api.API.Services
{
    public class OrderService : IOrderService
    {
        private readonly MemoryDbContext _context;
        private readonly IMemoryCache _memoryCache;
        private readonly ILogger<OrderService> _logger;

        public OrderService(MemoryDbContext context, IMemoryCache memoryCache, ILogger<OrderService> logger)
        {
            _context = context;
            _memoryCache = memoryCache;
            _logger = logger;
        }

        public IEnumerable<Order> AddOrder(Guid id, OrderViewModel orderViewModel)
        {
            try
            {
                if (_context.Orders.Any(o => o.Id == id))
                {
                    throw new InvalidOperationException("Já existe um pedido com o mesmo ID.");
                }

                if (orderViewModel.Itens == null || orderViewModel.Itens.Count == 0)
                {
                    throw new InvalidOperationException("A venda deve possuir pelo menos 1 item.");
                }

                var order = new Order(id, orderViewModel);
                _context.Orders.Add(order);
                SaveChanges();

                _memoryCache.Remove("Orders");

                var orders = _context.Orders.ToList();
                _logger.LogInformation("Order added successfully.");
                return orders;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error adding order: {ex.Message}");
                throw;
            }
        }

        public bool UpdateOrderStatus(Guid id, StatusEnum newStatus)
        {
            var existingOrder = _context.Orders
                .Include(o => o.OrderViewModel)
                    .ThenInclude(ovm => ovm.Itens)
                .FirstOrDefault(s => s.Id == id);

            if (existingOrder == null)
            {
                return false; 
            }

             switch (existingOrder.OrderViewModel.Status)
            {
                case StatusEnum.Aguardando_pagamento:
                    if (newStatus == StatusEnum.Pagamento_aprovado || newStatus == StatusEnum.Cancelado)
                    {
                        existingOrder.OrderViewModel.Status = newStatus;
                    }
                    else
                        throw new InvalidOperationException("Não foi possível atualizar o Status");
                    break;

                case StatusEnum.Pagamento_aprovado:
                    if (newStatus == StatusEnum.Enviado_transportadora || newStatus == StatusEnum.Cancelado)
                    {
                        existingOrder.OrderViewModel.Status = newStatus;
                    }
                    else
                        throw new InvalidOperationException("Não foi possível atualizar o Status");
                    break;

                case StatusEnum.Enviado_transportadora:
                    if (newStatus == StatusEnum.Entregue)
                    {
                        existingOrder.OrderViewModel.Status = newStatus;
                    }
                    break;

                default:
                    return false;
            }

            SaveChanges();

            _memoryCache.Remove("Orders");

            return true; 
        }

        public Order GetOrderById(Guid id)
        {
            return _context.Orders
                .Include(o => o.OrderViewModel)
                    .ThenInclude(ovm => ovm.Seller)
                .Include(o => o.OrderViewModel)
                    .ThenInclude(ovm => ovm.Itens)
                .FirstOrDefault(s => s.Id == id);
        }

        public IEnumerable<Order> GetOrders()
        {
            if (!_memoryCache.TryGetValue("Orders", out IEnumerable<Order> orders))
            {
                orders = _context.Orders
                    .Include(o => o.OrderViewModel.Seller)
                    .Include(o => o.OrderViewModel.Itens)
                    .ToList();

                _memoryCache.Set("Orders", orders, new MemoryCacheEntryOptions()
                    .SetAbsoluteExpiration(TimeSpan.FromHours(6))
                    .SetSlidingExpiration(TimeSpan.FromMinutes(10)));
            }

            return orders;
        }

        public bool RemoveOrder(Guid id)
        {
            var order = _context.Orders.FirstOrDefault(s => s.Id == id);

            if (order == null) return false;

            try
            {
                _context.Orders.Remove(order);
                SaveChanges();

                _memoryCache.Remove("Orders");
                _logger.LogInformation("Order removed successfully.");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error removing order: {ex.Message}");
                throw;
            }
        }

        private void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}
