﻿using Microsoft.EntityFrameworkCore;
using payment_api.Model.Order;

namespace payment_api.API.Data;

public class MemoryDbContext : DbContext
{
    public MemoryDbContext(DbContextOptions<MemoryDbContext> options) : base(options)
    {

    }

    public DbSet<Order> Orders { get; set; }
}
